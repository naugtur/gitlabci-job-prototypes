# Usage

1. install dev dependency

```
 npm install --save-dev gitlabci-job-prototypes
```

The package will create a folder called gitlabci-job-prototypes in the repo root, commit that.
The folder will also contain a VERSION file to track which version you have. When npm install updates the package it will overwrite the whole folder.

When you want to pull new changes
```
 npm update gitlabci-job-prototypes
```

2. use in gitlab-ci.yml via https://docs.gitlab.com/ee/ci/yaml/#include

```yml
include:
  - "/gitlabci-job-prototypes/dependencies.yml"
  - "/gitlabci-job-prototypes/build-gcr.yml"
  - "/gitlabci-job-prototypes/utils-gcr.yml"

dependencies-dev:
  variables:
    CURRENT_NODE_ALPINE: "node:10-alpine"
  extends: .s-npm-ci-dev
  before_script:
    - your custom modification
```

## Plugins

You can create folders in ./gitlabci-job-prototypes/ and they will be preserved when updating or installing this package.  
You can author plugins that install into specific folders inside and extend these jobs.

## Catalog of reusable jobs

### dependencies.yml

#### Current

 CURRENT_NODE_ALPINE must be based on alpine3.10+

| job                                | produces                                                                                                                                    | requires variables                                             |
| ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------- |
| .s-npm-ci-dev          | creates an artifact with node_modules to depend on in testing jobs                                                                          | CURRENT_NODE_ALPINE SKIP_BUILD_FROM_SOURCE                                   |
| .s-npm-ci-prod         | creates an artifact with node_modules to depend on in production build, onluy production deps are installed                                                    | CURRENT_NODE_ALPINE SKIP_BUILD_FROM_SOURCE                                   |
| .s-npm-ci-prv-ssh-dev  | with support for private packages over ssh to git; creates an artifact with node_modules to depend on in testing jobs                       | CURRENT_NODE_ALPINE SKIP_BUILD_FROM_SOURCE SSH_PRIVATE_KEY_BASE64 SSH_CONFIG |
| .s-npm-ci-prv-ssh-prod | with support for private packages over ssh to git; creates an artifact with node_modules to depend on in production build, , onluy production deps are installed | CURRENT_NODE_ALPINE SKIP_BUILD_FROM_SOURCE SSH_PRIVATE_KEY_BASE64 SSH_CONFIG |
| .s-audit-check | CURRENT_NODE_ALPINE | checks audif using npm-audit-resolver wrapper for npm audit
| .s-audit-check-production | CURRENT_NODE_ALPINE | checks audif using npm-audit-resolver wrapper for npm audit

`SKIP_BUILD_FROM_SOURCE` skiping rebuild is ok in majority of cases, it may be required for very few packages - if they ship with built code that's not compatible with alpine


#### Legacy

These jobs are still correct and not deprecated for any errors or security issues.  
They will work with node-alpine up to alpine 3.9  
Alpine 3.10+ doesn't have the `-y` option in apk.  

| job                                | produces                                                                                                                                    | requires variables                                             |
| ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------- |
| .s-setup-dependencies-dev          | creates an artifact with node_modules to depend on in testing jobs                                                                          | CURRENT_NODE_ALPINE                                   |
| .s-setup-dependencies-prod         | creates an artifact with node_modules to depend on in production build, checks the audit                                                    | CURRENT_NODE_ALPINE                                   |
| .s-setup-dependencies-prv-ssh-dev  | with support for private packages over ssh to git; creates an artifact with node_modules to depend on in testing jobs                       | CURRENT_NODE_ALPINE SSH_PRIVATE_KEY_BASE64 SSH_CONFIG |
| .s-setup-dependencies-prv-ssh-prod | with support for private packages over ssh to git; creates an artifact with node_modules to depend on in production build, checks the audit | CURRENT_NODE_ALPINE SSH_PRIVATE_KEY_BASE64 SSH_CONFIG |

### build-gcr.yml

| job              | produces                                                                                                                             | requires                                                                                 |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------- |
| .s-build-image   | builds ${BUILD_DOCKERFILE} or `Dockerfile` and uploads an image gcr.io/${GCR_CONTAINER_REGISTRY_NAME}/${BUILD_APP_NAME} and tags it with \${CI_COMMIT_SHA} | BUILD_APP_NAME GCR_CONTAINER_REGISTRY_NAME DOCKER_WRITER_GCR_JSON_BASE64                 |
| .s-build-image-expose-artifact   | builds ${BUILD_DOCKERFILE} or `Dockerfile` and creates an artifact in CI `image/image.tar` | BUILD_APP_NAME GCR_CONTAINER_REGISTRY_NAME DOCKER_WRITER_GCR_JSON_BASE64                 | 
| .s-push-image-to-registry   | uploads image/image.tar to gcr.io/${GCR_CONTAINER_REGISTRY_NAME}/${BUILD_APP_NAME} and tags it with \${CI_COMMIT_SHA} | BUILD_APP_NAME GCR_CONTAINER_REGISTRY_NAME DOCKER_WRITER_GCR_JSON_BASE64                 |
| .s-tag-named-tag | tags image gcr.io/${GCR_CONTAINER_REGISTRY_NAME}/${BUILD_APP_NAME}:${CI_COMMIT_SHA} with${BUILD_NAMED_TAG}                           | BUILD_APP_NAME GCR_CONTAINER_REGISTRY_NAME DOCKER_WRITER_GCR_JSON_BASE64 BUILD_NAMED_TAG ZONES_TO_TAG|

when using .s-build-image you must override `dependencies` param and pass the name of the job which produced production node_modules artefact. 

You can specify what docker file to be build using `BUILD_DOCKERFILE` variable. If omitted 'Dockerfile' is used.

You can add any extra arguments to docker using `DOCKER_EXTRA_ARGUMENTS`. 
includes a workaround from https://gitlab.com/gitlab-org/gitlab-runner/issues/4501#note_195033385 in version 1.2.1

To use the artifact produced by `s-build-image-expose-artifact` run `docker load -i image/image.tar` - it's what `s-push-image-to-registry` is also running

To tag an image in more than just the global gcr.io use `ZONES_TO_TAG` and pass a space-delimited list of zones to use with *.gcr.io - the image will become available in those zones (gcloud copies it)


```yml
include:
  - "/gitlabci-job-prototypes/dependencies.yml"
  - "/gitlabci-job-prototypes/build-gcr.yml"

dependencies-prod:
  variables:
    SKIP_BUILD_FROM_SOURCE: 1
    CURRENT_NODE_ALPINE: "node:10-alpine"
  extends: .s-npm-ci-prod

build-image:
  dependencies:
    - dependencies-prod
  extends: .s-build-image
  variables:
    BUILD_APP_NAME: "my-app"
    BUILD_DOCKERFILE: "Dockerfile.my-app"
    DOCKER_EXTRA_ARGUMENTS: '--build-arg SOME_ARG_KEY="SOME_ARG_VALUE" --custom_arg'
    ...

app-audit:
  dependencies:
    - dependencies-prod
  extends: .s-audit-check-production

tag-master:
  only:
    - master
  stage: after_test
  variables: 
    BUILD_APP_NAME: "my-app"
    BUILD_NAMED_TAG: "master"
    ZONES_TO_TAG: "eu asia"
  extends: .s-tag-named-tag
```

### utils-gcr.yml

| job              | produces                                                                                                                             | requires                                                                                 |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------- |
| .s-bucket-upload   | uploads a given file to a bucket cp ${LOCAL_FILE} gs://${DESTINATION_BUCKET} |  LOCAL_FILE DESTINATION_BUCKET  GC_WRITER_JSON_BASE64 |
| .s-bucket-upload-dir   | uploads a directory to a bucket cp -r ${LOCAL_PATH} gs://${DESTINATION_BUCKET} |  LOCAL_FPATH DESTINATION_BUCKET  GC_WRITER_JSON_BASE64 |

`LOCAL_FILE` and `LOCAL_PATH` can be a path with glob syntax (google cloud sdk supports that)

```yml
deploy-page:
  extends: .s-bucket-upload
  variables:
    GC_WRITER_JSON_BASE64: "credentials for writing the bucket"
    LOCAL_FILE: "index.html"
    DESTINATION_BUCKET: "websitebucket/website/main"
```

```yml
deploy-page:
  extends: .s-bucket-upload-dir
  variables:
    GC_WRITER_JSON_BASE64: "credentials for writing the bucket"
    LOCAL_FILE: "website"
    DESTINATION_BUCKET: "websitebucket/website/main"
```